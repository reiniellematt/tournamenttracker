﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Models;

namespace TrackerUI
{
    public partial class CreateTeamForm : Form
    {
        private List<PersonModel> availableTeamMembers = GlobalConfig.Connection.GetPerson_All();
        private List<PersonModel> selectedTeamMembers = new List<PersonModel>();
        private ITeamRequester callingForm;

        public CreateTeamForm(ITeamRequester caller)
        {
            InitializeComponent();
            // CreateSampleData();
            callingForm = caller;
            WireUpLists();
        }
        
        private void CreateSampleData()
        {
            availableTeamMembers.Add(new PersonModel { FirstName = "Tim", LastName = "Corey" });
            availableTeamMembers.Add(new PersonModel { FirstName = "Sue", LastName = "Storm" });


            selectedTeamMembers.Add(new PersonModel { FirstName = "Jane", LastName = "Smith" });
            selectedTeamMembers.Add(new PersonModel { FirstName = "Bill", LastName = "Jones" });
        }
        
        private void WireUpLists()
        {
            selectTeamDropdown.DataSource = null;

            selectTeamDropdown.DataSource = availableTeamMembers;
            selectTeamDropdown.DisplayMember = "FullName";

            teamMembersListBox.DataSource = null;

            teamMembersListBox.DataSource = selectedTeamMembers;
            teamMembersListBox.DisplayMember = "FullName";
        }

        private void CreateMemberBtn_Click(object sender, EventArgs e)
        {
            if(ValidateForm())
            {
                PersonModel p = new PersonModel();
                p.FirstName = firstNameTxtBox.Text;
                p.LastName = lastNameTxtBox.Text;
                p.EmailAddress = emailTxtBox.Text;
                p.CellphoneNumber = cellphoneTxtBox.Text;

                GlobalConfig.Connection.CreatePerson(p);

                selectedTeamMembers.Add(p);
                WireUpLists();

                firstNameTxtBox.Text = "";
                lastNameTxtBox.Text = "";
                emailTxtBox.Text = "";
                cellphoneTxtBox.Text = "";
            }
            else
            {
                MessageBox.Show("You need to fill in all of the fields.");
            }
        }

        private bool ValidateForm()
        {
            if(firstNameTxtBox.TextLength == 0)
            {
                return false;
            }

            if(lastNameTxtBox.TextLength == 0)
            {
                return false;
            }

            if(emailTxtBox.TextLength == 0)
            {
                return false;
            }

            if(cellphoneTxtBox.TextLength == 0)
            {
                return false;
            }

            return true;
        }

        private void AddMemberBtn_Click(object sender, EventArgs e)
        {
            PersonModel p = (PersonModel)selectTeamDropdown.SelectedItem;

            if (p != null)
            {
                availableTeamMembers.Remove(p);
                selectedTeamMembers.Add(p);

                WireUpLists();
            }
        }

        private void RemoveSelectedMemberBtn_Click(object sender, EventArgs e)
        {
            PersonModel p = (PersonModel)teamMembersListBox.SelectedItem;

            if (p != null)
            {
                availableTeamMembers.Add(p);
                selectedTeamMembers.Remove(p);

                WireUpLists(); 
            }
        }

        private void CreateTeamBtn_Click(object sender, EventArgs e)
        {
            TeamModel t = new TeamModel();

            t.TeamName = teamNameTxtBox.Text;
            t.TeamMembers = selectedTeamMembers;

            GlobalConfig.Connection.CreateTeam(t);

            callingForm.TeamComplete(t);

            this.Close();
            
            teamNameTxtBox.Text = "";
        }
    }
}
