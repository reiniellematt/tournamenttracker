﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.DataAccess;
using TrackerLibrary.Models;

namespace TrackerUI
{
    public partial class CreatePrizeForm : Form
    {
        private IPrizeRequester callingForm;

        public CreatePrizeForm(IPrizeRequester caller)
        {
            InitializeComponent();
            callingForm = caller;
        }

        private void CreatePrizeBtn_Click(object sender, EventArgs e)
        {
            if(ValidateForm())
            {
                PrizeModel model = new PrizeModel(placeNameTxtBox.Text, 
                                                  placeNumberTxtBox.Text,
                                                  prizeAmountTxtBox.Text,
                                                  prizePercentageTxtBox.Text);
                
                GlobalConfig.Connection.CreatePrize(model);

                callingForm.PrizeComplete(model);

                this.Close();

                placeNameTxtBox.Text = "";
                placeNumberTxtBox.Text = "";
                prizeAmountTxtBox.Text = "0";
                prizePercentageTxtBox.Text = "0";
            }
            else
            {
                MessageBox.Show("This form has invalid information. Please check and try again");
            }
        }

        private bool ValidateForm()
        {
            bool output = true;
            int placeNumber = 0;
            bool placeNumberIsValid = int.TryParse(placeNumberTxtBox.Text, out placeNumber);

            if(!placeNumberIsValid)
            {
                output = false;
            }

            if(placeNumber < 1)
            {
                output = false;
            }

            if(prizeAmountTxtBox.TextLength == 0)
            {
                output = false;
            }

            decimal prizeAmount = 0;
            double prizePercentage = 0;
            bool prizeAmountIsValid = decimal.TryParse(prizeAmountTxtBox.Text, out prizeAmount);
            bool prizePercentageIsValid = double.TryParse(prizePercentageTxtBox.Text, out prizePercentage);

            if (!prizeAmountIsValid || !prizePercentageIsValid)
            {
                output = false;
            }

            if (prizeAmount <= 0 && prizePercentage <= 0)
            {
                output = false;
            }

            return output;
        }
    }
}