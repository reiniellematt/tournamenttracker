﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    /// <summary>
    /// Represents the tournament.
    /// </summary>
    public class TournamentModel
    {
        /// <summary>
        /// Unique identifier for the tournament.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// The price of the entry fee.
        /// </summary>
        public decimal EntryFee { get; set; }

        /// <summary>
        /// The teams participating in the tournament.
        /// </summary>
        public List<TeamModel> EnteredTeams { get; set; } = new List<TeamModel>();

        /// <summary>
        /// Prizes to be won in this tournament.
        /// </summary>
        public List<PrizeModel> Prizes { get; set; } = new List<PrizeModel>();

        /// <summary>
        /// List of rounds that the tournament will contain.
        /// </summary>
        public List<List<MatchupModel>> Rounds { get; set; } = new List<List<MatchupModel>>();
    }
}
