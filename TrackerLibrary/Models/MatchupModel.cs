﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    /// <summary>
    /// Represents one match in the tournament.
    /// </summary>
    public class MatchupModel
    {
        /// <summary>
        /// The unique identifier for the matchup.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The set of teams involved in this match.
        /// </summary>
        public List<MatchupEntryModel> Entries { get; set; } = new List<MatchupEntryModel>();

        /// <summary>
        /// The winner of the match
        /// </summary>
        public TeamModel Winner { get; set; }

        /// <summary>
        /// Which round this match is part of.
        /// </summary>
        public int MatchupRound { get; set; }

        /// <summary>
        /// A readonly property which displays the matchup.
        /// </summary>
        public string DisplayName
        {
            get
            {
                StringBuilder output = new StringBuilder();

                foreach (MatchupEntryModel me in Entries)
                {
                    if (me.TeamCompeting != null)
                    {
                        if (output.Length == 0)
                        {
                            output.Append(me.TeamCompeting.TeamName);
                        }
                        else
                        {
                            output.Append($" vs. { me.TeamCompeting.TeamName }");
                        }
                    }
                    else
                    {
                        output.Append("Matchup not yet determined.");
                        break;
                    }
                }

                return output.ToString();
            }
        }
    }
}
