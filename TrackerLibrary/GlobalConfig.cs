﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.DataAccess;

namespace TrackerLibrary
{
    public static class GlobalConfig
    {
        public const string MatchupEntryFile = "MatchupEntryModels.csv";
        public const string MatchupFile = "MatchupModels.csv";
        public const string PeopleFile = "PersonModels.csv";
        public const string PrizesFile = "PrizeModels.csv";
        public const string TeamFile = "TeamModels.csv";
        public const string TournamentFile = "TournamentModels.csv";

        public static IDataConnection Connection { get; private set; }

        public static void InitializeConnections(DatabaseType db)
        {
            // Set up database connection.
            if(db == DatabaseType.SQL)
            {
                // TODO: Implement SQL Database in a later time.
                SQLConnector sql = new SQLConnector();
                Connection = sql;
            }

            // Set up connection to save data to text file.
            else if(db == DatabaseType.TextFile)
            {
                TextConnector text = new TextConnector();
                Connection = text;
            }
        }
    }
}
