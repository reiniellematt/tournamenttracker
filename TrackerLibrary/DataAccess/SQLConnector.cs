﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary.DataAccess
{
    // TODO: Implement SQL Database when you already know SQL.
    class SQLConnector : IDataConnection
    {
        public void CreatePerson(PersonModel model)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Saves a new prize to the database.
        /// </summary>
        /// <param name="model">The prize information.</param>
        /// <returns>The prize information, including the ID.</returns>
        public void CreatePrize(PrizeModel model)
        {
            throw new NotImplementedException();
        }

        public void CreateTeam(TeamModel model)
        {
            throw new NotImplementedException();
        }

        public void CreateTournament(TournamentModel model)
        {
            throw new NotImplementedException();
        }

        public List<PersonModel> GetPerson_All()
        {
            throw new NotImplementedException();
        }

        public List<TeamModel> GetTeams_All()
        {
            throw new NotImplementedException();
        }

        public List<TournamentModel> GetTournaments_All()
        {
            throw new NotImplementedException();
        }

        public void UpdateMatchup(MatchupModel model)
        {
            throw new NotImplementedException();
        }
    }
}
