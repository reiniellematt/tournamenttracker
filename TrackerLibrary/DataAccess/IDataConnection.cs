﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary.DataAccess
{
    public interface IDataConnection
    {

        void CreatePerson(PersonModel model);

        void CreatePrize(PrizeModel model);

        void CreateTeam(TeamModel model);

        void CreateTournament(TournamentModel model);
        
        void UpdateMatchup(MatchupModel model);

        List<PersonModel> GetPerson_All();

        List<TeamModel> GetTeams_All();

        List<TournamentModel> GetTournaments_All();
    }
}
